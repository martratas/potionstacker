package me.ford.potionstacker;

import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public class PotionStacker extends JavaPlugin {
	private int stackPotion;
	private int stackSplash;
	private int stackLingering;
	private InventoryListener listener = null;

	@Override
	public void onEnable() {
		loadConfiguration();
		changePotionStackSizes();
		getCommand("potionstackerreload").setExecutor(new ReloadCommand(this));
	}

	public void loadConfiguration() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		stackPotion = getConfig().getInt("potion", 2);
		stackSplash = getConfig().getInt("splash-potion", 2);
		stackLingering = getConfig().getInt("lingering-potion", 2);
		if (stackPotion != 1 || stackSplash != 1) {
			if (listener == null) {
				getServer().getPluginManager()
						.registerEvents(listener = new InventoryListener(this, stackPotion, stackSplash), this);
			} else {
				listener.setStacks(stackPotion, stackSplash);
			}
		} else {
			if (listener != null) {
				HandlerList.unregisterAll(listener);
				listener = null;
			}
		}
	}

	public void reload() {
		reloadConfig();
		loadConfiguration();
		changePotionStackSizes();
	}

	private void changePotionStackSizes() {
		changePotionStackSize("POTION", stackPotion);
		changePotionStackSize("SPLASH_POTION", stackSplash);
		changePotionStackSize("LINGERING_POTION", stackLingering);
	}

	private void changePotionStackSize(String potionName, int stack) {
		boolean potionReg = ReflectionUtil.setFor(potionName, stack);
		boolean potionNMS = ReflectionUtil.setForNMS(potionName, stack);
		if (potionReg && potionNMS) {
			getLogger().info("Set the maxStack size of " + potionName + " to " + stack);
		} else {
			getLogger().warning("Problem setting maxStack size of " + potionName + ".\n"
					+ "org.bukkit.Material:" + (potionReg ? "WORKED" : "FAILED") + "\n"
					+ "NMS.Items:" + (potionNMS ? "WORKED" : "FAILED"));
		}
	}

}
