package me.ford.potionstacker;

import java.util.EnumSet;

import org.bukkit.Material;
import org.bukkit.entity.Player;
//import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCreativeEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.ItemStack;

//import net.minecraft.server.v1_13_R2.EntityPlayer;

public class InventoryListener implements Listener {
	private final PotionStacker stacker;
	private EnumSet<Material> potions;
	public int stackPotion, stackSplash;
	boolean lookPotion, lookSplash;
	private final Material POTION;
	private final Material SPLASH_POTION;
	
	public InventoryListener(PotionStacker plugin, int potion, int splash) {
		stacker = plugin;
		stackPotion = potion;
		stackSplash = splash;
		POTION = Material.POTION;
		SPLASH_POTION = Material.matchMaterial("SPLASH_POTION"); // may be null in 1.8
		setupLook();
	}
	
	private void setupLook() {
		lookPotion = stackPotion > 1;
		lookSplash = stackSplash > 1;
		if (lookPotion && lookSplash) {
			if (SPLASH_POTION != null) {
				potions = EnumSet.of(POTION, SPLASH_POTION);
			} else {
				potions = EnumSet.of(POTION);
			}
		} else {
			if (lookPotion) {
				potions = EnumSet.of(POTION);
			} else { // should be at least one
				potions = EnumSet.of(SPLASH_POTION);
			}
		}
	}
	
	public void setStacks(int potion, int splash) {
		stackPotion = potion;
		stackSplash = splash;
		setupLook();
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		InventoryAction action = event.getAction();
		if (action == InventoryAction.CLONE_STACK ||
				action == InventoryAction.HOTBAR_SWAP ||
				action == InventoryAction.NOTHING) {
			return;
		}
		handleInventoryInteractEvent(event, event.getCurrentItem(), event.getCursor());
	}
	
	@EventHandler
	public void onIventoryDrag(InventoryDragEvent event) {
		handleInventoryInteractEvent(event, event.getOldCursor(), event.getCursor());
	}
	
	@EventHandler
	public void onInventoryCreative(InventoryCreativeEvent event) {
		handleInventoryInteractEvent(event, event.getCurrentItem(), event.getCursor());
	}

	private void handleInventoryInteractEvent(InventoryInteractEvent event, final ItemStack stack, final ItemStack alt) {
		final ItemStack cur;
		if (stack == null || stack.getType() == Material.AIR) {
			cur = alt;
		} else {
			cur = stack;
		}
		if (cur == null || !potions.contains(cur.getType())) {
			return;
		}
//		final EntityPlayer player = ((CraftPlayer) event.getWhoClicked()).getHandle();
		stacker.getServer().getScheduler().runTask(stacker, ()-> {
			ReflectionUtil.updateInventory((Player) event.getWhoClicked());
		});
	}

}
