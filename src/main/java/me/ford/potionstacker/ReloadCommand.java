package me.ford.potionstacker;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ReloadCommand implements CommandExecutor  {
	
	private final PotionStacker stacker;
	
	public ReloadCommand(PotionStacker plugin) {
		stacker = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		stacker.reload();
		sender.sendMessage(ChatColor.RED + "Config reloaded!");
		return true;
	} 
	
}