package me.ford.potionstacker;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public final class ReflectionUtil {
	private static String version = null;
	private static boolean post1dot17 = false;
	private static boolean post1dot18 = false;
	private static boolean post1dot18dot2 = false;
	private static boolean post1dot19 = false;
	private static boolean post1dot20 = false;

	// classes, methods and fields for setting NMS stack size
	private static Class<?> itemClass = null;
	private static Class<?> itemsClass = null;
	private static Method itemGetNameMethod = null; // for 1.17+

	// classes, methods and fields for updating player inventory
	private static Class<?> craftPlayerClass = null;
	private static Class<?> entityPlayerClass = null;
	private static Method getHandleMethod = null;
	private static Class<?> containerInterface = null;
	private static Method updateInventoryMethod = null;
	private static Field activeInventoryField = null;

	private ReflectionUtil() {
	}

	public static boolean setForClass(Class<?> clazz, Object instance, String fieldName, int value) {
		Field curField;
		try {
			curField = clazz.getDeclaredField(fieldName);
		} catch (NoSuchFieldException | SecurityException e1) {
			e1.printStackTrace();
			return false;
		}
		curField.setAccessible(true);
		try {
			curField.set(instance, value);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			return false;
		}
		curField.setAccessible(false);
		return true;
	}

	public static boolean setFor(String materialName, int stack) {
		Material potion;
		try {
			potion = Material.valueOf(materialName);
		} catch (IllegalArgumentException e) {
			return false;
		}

		return setForClass(Material.class, potion, "maxStack", stack);
	}

	private static boolean initiateNMSClasses() {
		try {
			itemClass = Class.forName(
					post1dot17 ? "net.minecraft.world.item.Item" : ("net.minecraft.server." + version + ".Item"));
			itemsClass = Class.forName(
					post1dot17 ? "net.minecraft.world.item.Items" : ("net.minecraft.server." + version + ".Items"));
			itemGetNameMethod = itemClass.getMethod(post1dot18 ? "a" : "getName");
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException e2) {
			e2.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean setForNMS(String materialName, int stack) {
		if (version == null) {
			version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
			post1dot20 = version.startsWith("v1_20");
			post1dot19 = version.startsWith("v1_19") | post1dot20;
			post1dot18dot2 = version.startsWith("v1_18_R2") || post1dot19;
			post1dot18 = version.startsWith("v1_18") || post1dot18dot2;
			post1dot17 = version.startsWith("v1_17") || post1dot18;
		}
		if (itemClass == null) {
			initiateNMSClasses();
		}
		Field potionField;
		try {
			potionField = itemsClass.getDeclaredField(materialName);
		} catch (NoSuchFieldException e) {
			if (post1dot17) {
				potionField = find1dot17PotionField(materialName);
			} else {
				potionField = null;
			}
			if (!post1dot17 || potionField == null) {
				e.printStackTrace();
				return false;
			}
		} catch (SecurityException e) {
			e.printStackTrace();
			return false;
		}
		Object potion;
		try {
			potion = potionField.get(null);// static
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			return false;
		}
		String fieldName;
		if (post1dot17 && !post1dot18dot2) {
			fieldName = "c";
		} else if (post1dot18dot2 && !post1dot19) {
			fieldName = "d";
		} else if (post1dot19) {
			fieldName = "d";
		} else {
			fieldName = "maxStackSize";
		}
		return setForClass(itemClass, potion, fieldName, stack);
	}

	/**
	 * Gets the field for a specific type of potion in 1.17 and above.
	 *
	 * It turns out that in those cases, the material name is not really
	 * human-readable. Because the name may change in while the class is obfuscated,
	 * the current behaviour is to find the corresponding field manually.
	 *
	 * While this is quite a tedious process, it only happens once (when the plugin
	 * initializes) which means it's not a huge issue.
	 *
	 * @param materialName
	 * @return
	 */
	private static Field find1dot17PotionField(String materialName) {
		for (Field field : itemsClass.getFields()) {
			if (!Modifier.isStatic(field.getModifiers())) {
				continue; // only static field
			}
			Object got;
			try {
				got = field.get(null);
			} catch (IllegalArgumentException | IllegalAccessException e1) {
				e1.printStackTrace(); // should not happen
				continue;
			}
			if (got == null) {
				continue; // ignore null
			}
			if (!itemClass.isInstance(got)) {
				continue; // wrong type
			}
			String name;
			try {
				name = (String) itemGetNameMethod.invoke(got);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| ClassCastException e) {
				e.printStackTrace();
				continue;
			}
			String[] split = name.split("\\.");
			name = split[split.length - 1]; // last
			if (name.equalsIgnoreCase(materialName)) {
				return field;
			}
		}
		return null;
	}

	private static boolean initiateClassesForInventoryUpdate() {
		try {
			craftPlayerClass = Class.forName("org.bukkit.craftbukkit." + version + ".entity.CraftPlayer");
			entityPlayerClass = Class.forName(post1dot17 ? "net.minecraft.server.level.EntityPlayer"
					: ("net.minecraft.server." + version + ".EntityPlayer"));
			getHandleMethod = craftPlayerClass.getMethod("getHandle");
			containerInterface = Class.forName(post1dot17 ? "net.minecraft.world.inventory.Container"
					: "net.minecraft.server." + version + ".Container");
			updateInventoryMethod = entityPlayerClass.getMethod("updateInventory", containerInterface);
			activeInventoryField = entityPlayerClass.getField("activeContainer");
		} catch (ClassNotFoundException | NoSuchFieldException | SecurityException | NoSuchMethodException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean updateInventory(Player bukkitPlayer) {
		if (version == null) {
			version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
			post1dot17 = version.startsWith("v1_17") || version.startsWith("v1_18") || version.startsWith("v1_19");
		}
		if (post1dot17) {
			return true; // no longer needed
		}
		if (craftPlayerClass == null) {
			initiateClassesForInventoryUpdate();
		}
		Object craftPlayer = craftPlayerClass.cast(bukkitPlayer);
		try {
			Object player = getHandleMethod.invoke(craftPlayer);
			Object activeContainer = activeInventoryField.get(player);
			updateInventoryMethod.invoke(player, activeContainer);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}
}
